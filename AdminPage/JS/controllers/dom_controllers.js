export let DOM_CONTROLLER = {
  DOM: (...elementID) => {
    return elementID.map((id) => {
      return document.querySelector(id);
    });
  },
  getValueOnClick: (value) => {
    return value;
  },
};
