import { BrandInput } from "./BrandInput.js";
import { DisplayInput } from "./DisplayInput.js";
import { ImageInput } from "./ImageInput.js";
import { NameInput } from "./NameInput.js";
import { PriceInput } from "./PriceInput.js";
import { RamInput } from "./RAMInput.js";
import { RomInput } from "./RomInput.js";

export const ProductModalBody =
  BrandInput +
  NameInput +
  ImageInput +
  PriceInput +
  RamInput +
  RomInput +
  DisplayInput;
