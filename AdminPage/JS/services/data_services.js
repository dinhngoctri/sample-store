const BASE_URL = "https://62b07878e460b79df0469b79.mockapi.io/SampleStore";

export let DATA_SERVICES = {
  getData: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  addProduct: (product) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: product,
    });
  },
  deleteProduct: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  updateProduct: (product) => {
    return axios({
      url: `${BASE_URL}/${product.id}`,
      method: "PUT",
      data: product,
    });
  },
};
