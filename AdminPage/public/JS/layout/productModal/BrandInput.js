var BrandInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <label class="input-group-text">Brand</label>
    </div>

    <select class="custom-select" id="product-brand">
        
        <option value="">
        Please select one of the options below
        </option>

        <option
        value="Apple"
        brandLogoImage="./Image/apple-logo.png"
        brandContainerBackground="rgb(178, 178, 179)"
        >
        Apple
        </option>

        <option
        value="Samsung"
        brandLogoImage="./Image/samsung-logo.png"
        brandContainerBackground="white"
        >
        Samsung
        </option>
        
        <option
        value="Google"
        brandLogoImage="./Image/google-logo.png"
        brandContainerBackground="white"
        >
        Google
        </option>
    
    </select>
</div>

<span
id="brand-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;
