var NameInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Product name</span>
    </div>
    
    <input type="text" id="product-name" class="form-control" />
</div>

<span
id="name-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;
