var RamInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">RAM</span>
    </div>

    <input type="text" id="ram" class="form-control" />

    <div class="input-group-append">
        <span class="input-group-text">GB</span>
    </div>
</div>

<span
id="ram-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;
