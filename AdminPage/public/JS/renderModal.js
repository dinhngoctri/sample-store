const BrandInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <label class="input-group-text">Brand</label>
    </div>

    <select class="custom-select" id="product-brand">
        
        <option value="">
        Please select one of the options below
        </option>

        <option
        value="Apple"
        brandLogoImage="./Image/apple-logo.png"
        brandContainerBackground="rgb(178, 178, 179)"
        >
        Apple
        </option>

        <option
        value="Samsung"
        brandLogoImage="./Image/samsung-logo.png"
        brandContainerBackground="white"
        >
        Samsung
        </option>
        
        <option
        value="Google"
        brandLogoImage="./Image/google-logo.png"
        brandContainerBackground="white"
        >
        Google
        </option>
    
    </select>
</div>

<span
id="brand-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;

const DisplayInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Display describe</span>
    </div>

    <textarea
    id="display"
    class="form-control"
    maxlength="50"
    aria-label="With textarea"
    ></textarea>
</div>
`;

const ImageInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Image</span>
    </div>
    
    <input type="text" id="product-image" class="form-control" />
</div>
            
<span
id="image-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;

const NameInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Product name</span>
    </div>
    
    <input type="text" id="product-name" class="form-control" />
</div>

<span
id="name-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;

const PriceInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Price</span>
    </div>

    <input type="text" id="price" class="form-control" />

    <div class="input-group-append">
        <span class="input-group-text">$</span>
    </div>
</div>

<span
id="price-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;

const RamInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">RAM</span>
    </div>

    <input type="text" id="ram" class="form-control" />

    <div class="input-group-append">
        <span class="input-group-text">GB</span>
    </div>
</div>

<span
id="ram-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;

const ROMArray = ["64GB", "128GB", "256GB", "512GB", "1TB", ""];
let contentHTML = "";
ROMArray.map((item) => {
  const radioButton = `
    <div class="form-check form-check-inline col-4 m-0 p-0">
            
        <input
        class="form-check-input"
        type="radio"
        name="rom-measure-radio"
        value="${item}"
        onclick="getRomValue(this.value)"
        ${item !== "" ? "" : "checked"}
        />
      
        <label class="form-check-label">
        ${item !== "" ? (item = item) : (item = "Unknown")}
        </label>
                
    </div>
    `;
  contentHTML += radioButton;
});

const RomInput = `
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="rom">ROM</label>
    </div>
    
    <div class="form-control h-100">
        
        <div class="w-100 row px-3">
           ${contentHTML}   
        </div>

    </div>
</div>
`;

const ProductModalBody =
  BrandInput +
  NameInput +
  ImageInput +
  PriceInput +
  RamInput +
  RomInput +
  DisplayInput;

document.getElementById("product-modal-body").innerHTML = ProductModalBody;
