export const PRODUCT_CONTROLLERS = {
  returnBrandContainerHeightArray: (containerHeightArray) => {
    let container = document.getElementsByName("brand-container");
    container.forEach((item) => {
      containerHeightArray.push(item.offsetHeight);
    });
  },

  brandFilter: (containerHeightArray) => {
    let brandCheckbox = document.getElementsByName("brand-checkbox");
    let brandContainer = document.getElementsByName("brand-container");

    // Hide and show brand container
    brandCheckbox.forEach((item, index) => {
      if (item.checked === true) {
        brandContainer[index].style.height = `${containerHeightArray[index]}px`;
        brandContainer[index].style.marginBottom = "3rem";
        brandContainer[index].style.left = "0";
      } else {
        brandContainer[index].style.height = "0px";
        brandContainer[index].style.marginBottom = "0rem";
        brandContainer[index].style.left = "-100vw";
      }
    });
  },

  showAllBrandContainer: (containerHeightArray) => {
    let checkbox = document.getElementById("show-all-checkbox");
    let brandCheckbox = document.getElementsByName("brand-checkbox");
    let brandContainer = document.getElementsByName("brand-container");

    if (checkbox.checked === true) {
      brandContainer.forEach((item, index) => {
        // Others checkbox will be unchecked
        brandCheckbox[index].checked = false;

        // Then show all the brand containers
        item.style.height = `${containerHeightArray[index]}px`;
        item.style.marginBottom = "3rem";
        item.style.left = "0";
      });
    }

    // Disabled the A-L-L checkbox
    checkbox.disabled = true;
  },

  renderPhoneList: (data) => {
    let brandArr = [];
    let stopFindIndex = null;

    data.map((item) => {
      let index = 0;

      // Prevent program runs into findIndex loop if the previous brand name equal with the current
      if (item.brand != stopFindIndex) {
        index = brandArr.findIndex((phone) => {
          stopFindIndex = item.brand;
          return phone == item.brand;
        });
      }

      let sortCheckbox = "";
      let containerContent = "";

      if (index == -1) {
        brandArr.push(item.brand);

        sortCheckbox = `
          <div class="form-check mr-4">
            <label class="form-check-label">
              <input
                type="checkbox"
                class="form-check-input"
                name="brand-checkbox"
                onclick="brandFilter(event)"
              />
                ${item.brand}
            </label>
          </div>`;

        containerContent = `
          <div class="product-cover" name="brand-container">
              <div class="product-brand-logo" style="background-color:${item.brandContainerBackground}">
                  <img src=${item.brandLogoImage} alt="" />
              </div>
              <div id="${item.brand}-phone-container" class="product-container"></div>
          </div>`;
      }

      let itemContent = `
        <div class="product-item">
          <img src=${item.img} alt="..." />
      
          <div class="product-content">
            <h5>${item.name}</h5>
      
            <div class="product-specification">
              <ul>
                <li>
                  <span>RAM:</span>
                  <span>${item.ram}</span>
                </li>
                <li>
                  <span>ROM:</span>
                  <span>${item.rom}</span>
                </li>
                <li>
                  <span>Display:</span>
                  <span>${item.display}</span>
                </li>
              </ul>
            </div>
      
            <div class="price flex-between-center">
              <span class="font-weight-bold">${
                item.price * 1 === item.price
                  ? `From $${item.price}`
                  : `${item.price}`
              }</span>
      
              <button class="btn" style="display:${
                item.price * 1 === item.price ? `inline-block` : `none`
              }"
              onclick="addProductToCart(${item.id})">
              Buy
              </button>
            </div>
          </div>
        </div>`;

      document.getElementById("sort-container").innerHTML += sortCheckbox;
      document.getElementById("product-big-list").innerHTML += containerContent;
      document.getElementById(`${item.brand}-phone-container`).innerHTML +=
        itemContent;
    });
  },
};
