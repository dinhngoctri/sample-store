import { CART_CONTROLLERS } from "./controllers/cart_controllers.js";
import { DATA_CONTROLLERS } from "./controllers/data_controllers.js";
import { NOTICE_CONTROLLERS } from "./controllers/notice_controllers.js";
import { PRODUCT_CONTROLLERS } from "./controllers/product_controllers.js";
import { DATA_SERVICES } from "./services/data_services.js";

let apiData = [];
let containerHeightArray = [];
let brandShowing = 0;
let cart = CART_CONTROLLERS.getDataFromLocalStorage();

let renderHTML = () => {
  DATA_SERVICES.getData()
    .then((success) => {
      apiData = success.data;
      PRODUCT_CONTROLLERS.renderPhoneList(
        DATA_CONTROLLERS.sortData(DATA_CONTROLLERS.replaceData(apiData))
      );
      // get the height of brand container to run the effect when filter
      PRODUCT_CONTROLLERS.returnBrandContainerHeightArray(containerHeightArray);
    })
    .catch((error) => {});
};
renderHTML();

/**
 *
 */

let brandFilter = (event) => {
  // Get the target checkbox
  let checkbox = event.target;

  let brandContainer = document.getElementsByName("brand-container");
  let showAllCheckbox = document.getElementById("show-all-checkbox");

  // showAllCheckbox is the element that show all the brand container
  showAllCheckbox.checked = false;
  showAllCheckbox.disabled = false;

  // check if all the brand container is showing or hiding
  checkbox.checked === true ? brandShowing++ : brandShowing--;

  // if it's true, unchecked all the brand checkbox and checked the showAllCheckbox
  // else, running normally
  if (brandShowing === 0 || brandShowing === brandContainer.length) {
    showAllCheckbox.checked = true;
    brandShowing = 0;
    PRODUCT_CONTROLLERS.showAllBrandContainer(containerHeightArray);
  } else {
    PRODUCT_CONTROLLERS.brandFilter(containerHeightArray);
  }
};
window.brandFilter = brandFilter;

/**
 *
 */

let showAllBrandContainer = () => {
  brandShowing = 0;
  PRODUCT_CONTROLLERS.showAllBrandContainer(containerHeightArray);
};
window.showAllBrandContainer = showAllBrandContainer;

/**
 *
 */

let addProductToCart = (id) => {
  CART_CONTROLLERS.addProductToCart(apiData, id, cart);
  CART_CONTROLLERS.updateLocalStorage(cart);

  // show a notice when adding a product into cart
  NOTICE_CONTROLLERS.toastNotice(cart, id);
};
window.addProductToCart = addProductToCart;

/**
 *
 */

CART_CONTROLLERS.renderCart(cart);

/**
 *
 */

let deleteProductInCart = (id) => {
  CART_CONTROLLERS.deleteProductInCart(cart, id);
  CART_CONTROLLERS.updateLocalStorage(cart);
};
window.deleteProductInCart = deleteProductInCart;

/**
 *
 */

let changeQuantity = (id, step) => {
  CART_CONTROLLERS.changeQuantity(cart, id, step);
  CART_CONTROLLERS.updateLocalStorage(cart);
};
window.changeQuantity = changeQuantity;

/**
 *
 */

let clearCart = () => {
  CART_CONTROLLERS.clearCart(cart);
  CART_CONTROLLERS.updateLocalStorage(cart);
};
window.clearCart = clearCart;

/**
 *
 */

let purchase = () => {
  NOTICE_CONTROLLERS.purchase();

  // setTimeout to close the modal, if not the modal can't be closed
  setTimeout(() => {
    clearCart();
  }, 1);
};
window.purchase = purchase;
